#!/bin/bash
#
# Fantasy Grounds Session Decal Generator
# by GM Wintermute (https://www.gmwintermute.com)
#
# You are free to modify this script.  Just give me credit by leaving in my announcement text.  Thanks
#


#Which font to use
SESFONT=/usr/share/fonts/truetype/mason-sans/MasonSansRegular_Regular.ttf



#Text color of the SESTEXT and session number
TEXTCOLOR=white

#Name of session template image
TEMPLATEIMAGE=template.jpg
SESTEXT=Session

#Do not edit below this line.

BEGINNUM=$1
ENDNUM=$2

for i in `seq $BEGINNUM $ENDNUM`;
do
echo "Generating image #$i of $ENDNUM"
convert template.jpg  -font $SESFONT -pointsize 125  -draw "gravity south fill black  text -300,349 '$SESTEXT $i' fill black  text -300,350 '$SESTEXT $i' fill $TEXTCOLOR  text -300,351 '$SESTEXT $i' " graphics/decals/$i.jpg
done
